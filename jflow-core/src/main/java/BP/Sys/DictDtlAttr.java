package BP.Sys;

import BP.DA.*;
import BP.En.*;
import BP.*;
import BP.Web.*;
import java.util.*;

/** 
  配置信息
*/
public class DictDtlAttr extends EntityMyPKAttr
{
/// <summary>
	/// 关联的外键
	/// </summary>
	public static final String DictMyPK = "DictMyPK";
	public static final String BH = "BH";
	public static final String Name = "Name";
	public static final String ParentNo = "ParentNo";
	/// <summary>
	/// 属性Key
	/// </summary>
	public static final String TableID = "TableID";
	/// <summary>
	/// 列选择
	/// </summary>
	public static final String OrgNo = "OrgNo";
	/// <summary>
	/// 顺序号
	/// </summary>
	public static final String Idx = "Idx";
}